#include <stdio.h>

int main()
{
  int n,i;
  float product = 1;

  printf("Enter n\n");
  scanf("%d",&n);

  for (i=1; i<=n; i=i+1)
    product = product * i/(2.0*i-1);

  printf("Product = %f\n",product);

  //system("pause");
  return(0);
}
