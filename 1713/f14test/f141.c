#include <stdio.h>
#include <math.h>

int main()
{
  double stddev;  //standard deviatiom
  int i;
  double num[6];
  double average;
  double sum=0;

  printf("Enter 6 doubles\n");
  for (i=0; i<6; i++)
    scanf("%lf",&num[i]);

  for (i=0; i<6; i++)
    {
      sum=sum+num[i];
    }
  average = sum/6;

  sum = 0;
  for (i=0; i<6; i++)
    {
      sum = sum + (num[i]-average)*(num[i]-average);
    }
  stddev = sqrt(sum/6);


  printf("Average is %lf\n",average);
  printf("Standard Deviation is %lf\n",stddev);
  return(0);
}
