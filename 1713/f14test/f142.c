#include <stdio.h>
#include <stdlib.h>

int main()
{
  int a[7]={1,1,0,0,0,0,0};
  int i=2;

  while (i<7)
   {
     if (i<5)	
       a[i] = a[i-1]+a[i-2];
     else
       a[i] = a[i-3];
     i = i + 1;
   }

  for (i=0; i<7; i++)
    printf("a[%d] = %d\n",i,a[i]);
}
