#include <stdio.h>
#include <math.h>

int Powerof3(int n)
{
  int i=1,power;

  power = 3;
  while (power<n)
   {
    i = i + 1;
    power = power * 3;
   }

  if (power == n)
    return(1);
  else
    return(0);
}

int main()
{
  int n;

  printf("Enter n\n");
  scanf("%d",&n);

  if (Powerof3(n) == 1)
    printf("n is a power of 3\n");
  else
    printf("n is not a power of 3\n");

  return(0);
}
