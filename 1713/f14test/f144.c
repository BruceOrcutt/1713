#include <stdio.h>

int function1(int x)
{
  return(2*x+1);
}

int main()
{
  int j=3;

  while (j < 50)
    {
      if (j<10)
	j = function1(j-1);
      else
	j = function1(j)-1;
      printf("%d\n",j);
    }

  return(0);
}
