#include <stdio.h>



         //////////////////////////////
        // Program: assign4
       //  By:      Bruce A Orcutt (csz860)
      //   Date:    3/3/2017
     //    Purpose: given a file of integers, and an integer, return the request number 
    //      of largest numbers.
   ////////////////////////////////          



int find_spot(int items[],int value, int size) 
// find_spot
// purpose: see if a value has a slot in the largest values array
// parameters:
// 	items[]: array holding the largest items found.  
//	value:   value to insert if it's larger than any other value in array
//	size:    size of the array
// returns:
//	>=0:	 index to the array where to insert the value
//	-1:      flag that all values in the array are larger  

{
	int i;

	for(i = 0;i<size;i++) 
		if (value > items[i]) return i;
	return -1;	
} //end find_spot  

void insert_item(int items[],int index,int value, int size) 
// insert_item
// purpose: insert a value at a provided index in the array
// parameters:
//	items[]: the array of values to insert into
//      index:   where to install the value into the array
//      value:   the value to insert
//      size:    size of the array
{
	int i     = 0; 
	int temp  = value; 
        int temp2 = 0;

	if (index == -1) return;  // EXIT POINT: -1 is a signal no need to insert

	for(i = index; i < size; i++) {
		temp2    = items[i];
		items[i] = temp;
		temp     = temp2; 
	}
}


int main(int argc, char *argv[]) {
	int   i,k = 0;
        FILE *F;
	int   temp = 0;
/////////
 // argument parsing and validation 
////////
	// ensure proper number of arguments
	if(argc != 3) {
		printf("ERROR: must have two arguments\n");
		printf("Usage: %s filename size\n",argv[0]);
		return -1;  // EXIT POINT
	}
  
	// open the file specified by the user
	if (!(F = fopen(argv[1],"r"))) {
		printf("ERROR: unable to open file %s\n",argv[1]);
		return -3;  // EXIT POINT
	}

	// Must have at least one element to process
	if ((k = atoi(argv[2])) < 1) {
		printf("ERROR, size must be at least 1\n");
		return -2;  // EXIT POINT
	}
 
//////////
// initialization
///////// 
	int largest[k];  // holds the larges k elements we find
	
	// initialize the array
	for(i = 0; i < k; i++) 
		largest[i] = 0;

///////////
// process data
/////////
	while(!feof(F)) {
		fscanf(F,"%d",&temp);
		insert_item(largest,find_spot(largest,temp,k),temp,k);
	}  

	// now print the results
	for(i = 0; i < k; i++ ) 
		printf("%d ",largest[i]);

	printf("\n");
	
	return 0; // EXIT POINT
 
  }

  

