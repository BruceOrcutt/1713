#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//////////////////
// assign0
// By Bruce A Orcutt
// For CS 1713
//
// Program takes in one parameter, a file of integers
// then adds the numbers, and reports the sum
//
// NOTE: besides comments, and variable initialization, 
//  code is a copy and paste of provided code

int main(int argc, char *argv[]) {
	int   total = 0; // total of the ints
	int   num   = 0; // holds each read number
        FILE *fp;        // File pointer for handling file passed in as argument     

       // Check number of command line arguments
	if ( argc != 2 ) { 
		fprintf(stderr,"Usage: %s <filename>\n", argv[0]); 
		exit(-1); // EXIT POINT
	} // end of if 

	// Open file given on command line
	fp = fopen(argv[1],"r");

	if ( fp == NULL ) {
		perror(argv[1]);
		exit(-1); // EXIT POINT
	} // end of if

	// Read one number at a time and sum all numbers in file
	total = 0;
	while( fscanf(fp,"%d",&num) == 1 ) { // while scanf is able to find another int
		 total += num; // add the current int to the total
	} // end of while
	
	// Print out total on stdout
	printf("Total = %d\n",total);
	
	exit(0); // EXIT POINT

} // end of main
