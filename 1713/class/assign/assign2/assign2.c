#include <stdio.h>
#include <math.h>

  /////////////////////////////////
 // Program: Assign2.c
//  By: Bruce A. orcutt (csz860)
//  Use: Prints the list of roman numerals, from 1 to 1000 to STDOUT


void  print_letters(int  value,     // value to convert
		    int  stepping,  // level to convert (1s, 10s, 100s)
		    char first,     // char representing lowest item of the range
		    char middle,    // the middle character of the range
		    char end)  {    // next highest character

  int temp  = 0;   // for calculating which case we are in
  int index = 0;   // for looping repeating values


  switch(temp = value/stepping) {
    // for 1st three, just repeat the character how many times needed
     case 1:
     case 2:
     case 3:
       for(index = 0; index < temp; index++) {
	 printf("%c",first);
       } 
       break;

     // for case 4. it's one first less than the middle
     case 4:
       printf("%c%c",first,middle);
       break;

     // case 5 is the middle case
     case 5: 
       printf("%c",middle);
       break;

     // case for 6,7,8 are the same as 1,2,3, but have to subtract 5 to get number of chars to print
     case 6:
     case 7:
     case 8:
       printf("%c",middle);
       temp -= 5;
       for(index = 0; index < temp; index++) {
	 printf("%c",first);
       }
       break;

     // case 9 is just like case 4, but the first and end characters
     case 9:
       printf("%c%c",first,end);
      
  }; // end switch

  
} //end print_number


int main(void) {
  int index        = 1;       // for loop index, also is our number we are converting
  int value        = 0;       // value we are currently working on converting
  
  
  for(index = 1; index < 1001; index++) {
    // print the number being converted
    printf("%d ",index); 
 
    // save the value, so we can be destructive   
    value = index;

    // since limit is 1000, if 1000, end
    if(index == 1000) {
      printf("M");
      value = 0;
    }  // end if

    // get the steps of 100s
    print_letters(value,100,'C','D','M'); 
    value %= 100;

    // get the steps of 10s
    print_letters(value,10,'X','L','C');
    value %= 10;    

    // get the steps of 1s
    print_letters(value,1,'I','V','X');
    
    // provide a newline
    printf("\n");
  } // end for

      
} // end main


