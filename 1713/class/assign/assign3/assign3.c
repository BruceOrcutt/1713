#include <stdio.h>
#include <math.h>
#include <stdlib.h>

     /////////////////////////
    // Program: assign3
   //  By: Bruce Orcutt (csz860)
  //   Function: Estimates e to the power of an int, using the Taylor series.
 //    Input: The number of steps of estimation taken and the power of e to estimate
//     Special note: requires -lm to compile, due to use of the pow function in math.h
//////////////////////////


double factorial(int n) {
  // Calculates the factorial of a given number. Double used due to possible overflow possibility with int
  int     index = 0;
  double  value = 1;  // need to start at 1, or else multiply will be all zeroed out!

  for(index = 1; index <= n; index++) {
    value *= index;
  }
  
  return(value);
}  // end of factorial


double exponent(double x,
		int    n) {
  // Estimates e to the power of a given number, for n number of estimation iterations  
  int    index = 0;
  double value = 0.0;


  for(index = 0; index <= n; index++) {
    value += pow(x,index) / factorial(index);
    printf("\t%d\t%.10lf\n",index,value);
  }
  
  return(value);
  
} // end of exponent

int main(int   argc,    // number of arguments
	 char *argv[]) {  // the arguments themselves

  double d = 0.0;
  int    i = 0;

  // ensure proper usage
  if (argc < 2) {
    fprintf(stderr,"Usage: %s int double\n",argv[0]);
    exit(-1);
  }

  // sscanf, scans a string instead of STDIN
  sscanf(argv[1],"%d",&i);
  sscanf(argv[2],"%lf",&d);

  printf("Requested: e to the power of %lf\n\n",d);
  printf("\ti\tApproximation\n");
  printf("----------------------------------------\n");
  exponent(d,i);

  printf("Exact value = %.10lf\n",exp(d));
  
  
} // end of main
