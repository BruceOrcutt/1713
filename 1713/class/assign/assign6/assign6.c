#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 1000
#define LINESIZE 1000

typedef struct wordfreq {
        int count;
        char *word;
} wfb;

int main(int argc,char *argv[]) {
        wfb *a[1000];
        int i,done,count,added;
        char temp[LINESIZE];
        FILE *F,*G;

        // check for proper arguments
        if(argc < 3) {
                printf("ERROR: proper use: %s inputfile outputfile\n",argv[0]);
                exit(-1);    // EXIT POINT
        }

        // open both files
        if(!(F = fopen(argv[1],"r"))) {
        	printf("ERROR opening files!\n");
                exit(-2);   // EXIT POINT
        }
	if(!(G = fopen(argv[2],"w"))) {
		printf("ERROR opening files!\n");
		exit(-3);   // EXIT POINT
	}

        // initialization
        for(i=0;i<SIZE;i++)
                a[i] = NULL;

        done  = 0;
        count = 0;
	added = 0;

        // cycle through the data in the file
        while (!done) {
		fgets(temp,LINESIZE,F);

		// check for end of file
		if (feof(F)) done = 1;

		if (!done) {
			added = 0;
			i = 0;
			for (i = 0; i < count; i++) {
				if (strcmp(a[i]->word,temp) == 0) {
					a[i]->count++;
					i = count;
					added++;
				} // end if
			} // end for

			if (!added) {
				// add space for struct plus length of string
				a[i] = (wfb *)calloc(1,sizeof(wfb));
				a[i]->word = (char *)calloc(1,strlen(temp)+1);
				a[i]->count = 1;
				strcpy(a[i]->word,temp);
				count++;
			} // end if added
		} // end if

        } // end while

	// print the results to the file
	for(i = 0; i < count;i++) {
		fprintf(G,"%d %s",a[i]->count,a[i]->word);
	}

	// close and clean up
        fclose(F);
	fclose(G);
	done = 0;
	i = 0;
	
	while(!done) {
		if(a[i] == NULL) done = 1;
		else {
			free(a[i]->word);
			free(a[i]);
		}
		
		i++;
	}

        return 0;
}
