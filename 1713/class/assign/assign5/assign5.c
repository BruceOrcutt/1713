#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 5000

     /////////////////////////////
    // Program: assign5
   // By: 	Bruce Orcutt
  // Date:      3/22/2017
 // Purpose:    Given a string, find and print the longest palindrome contained
/////////////////////////////////


int palindromelength(char *str, int i, int j) {
// Calculates the length of a palindrome.
// i and j are indexes into the string.
// returns the length of the palindrom
// returns -1 if not a palindrome
	int a,b,count = 0;

	// count in both directions.  Each character should equal eachother	
	for(a=i,b=j;a<b;a++,b--) 
		if (str[a] != str[b]) return -1;
		else count++; 
	return count*2;	

}


int main(void) {
	char string[SIZE];  // string to read and analyze
	int x,y,max,fx,fy = 0; // x and y are loop indexes
			       // max is length of palindrome found so far
			       // fx,fy are the final x and y indicies into string for the largest value


	// get the strong from the user
	printf("Enter a string\n"); 
	fgets(string,sizeof(string),stdin);

	// compare each substring by iterating over the string for each character
	for(x=0; x<strlen(string)-1; x++)
		for(y=x;y<strlen(string); y++)
			// if we found a new largest palindrome
			if (palindromelength(string,x,y)>max) {
				max = palindromelength(string,x,y);
				fx = x;
				fy = y;
			}  	

	// terminate the string at the end of the largest palindrome	
	string[fy+1] = '\0';

	// largest palindrome starts at fx positions from the start of string
	printf("Largest palindrome is %s\n",string+fx);

}
