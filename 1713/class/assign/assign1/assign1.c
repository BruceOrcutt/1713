#include <stdio.h>

int main(void) {
  
  //Program:   Assignment1
  //For:       CS1713
  //By:        Bruce A. Orcutt
  //abc123:    csz860
  //Purpose:   This program writes the source code for another program. 
  //The generated program will display a table of degrees and equivalent radian
  //values, in incremements of 10.

  int degrees = 0;

  printf("#include <stdio.h>\n");
  printf("#define PI 3.141593\n");

  printf("int main(void) {\n\n");
  printf("    int degrees = 0;\n");
  printf("    double radians;\n");

  printf("printf(\"Degrees to Radians \\n\");");

  for(degrees = 0; degrees < 361; degrees += 10) {
    printf("    degrees = %d;\n",degrees);
    printf("    radians = degrees*PI/180;\n");
    printf("    printf(\"%%6i %%9.6f \\n\", degrees, radians);\n\n");
  }

  printf("\n}\n");
 
}
