#include <stdio.h>
#include <stdlib.h>

#define SIZE 1000  // max number of primes array
#define MSTOP 32   // maximum multiple to consider

     /////////////////////////
    // Program: assign7
   // By: Bruce Orcutt
  // Date: 4/17/17
 // Purpose: Generate a linked list of ints.  Reduce it to just prime numbers.
/////////////////////////

// basic data structure
struct Node {
        int num;
        struct Node *next;
};

// syntactic sugar
typedef struct Node node;

// print out the list
void printList(node *head) {
	node *cur;
	int count;


	count = 0;
	cur = head;
	while (cur->next != NULL) {
		count++;
		printf("%d\t",cur->num);
		cur = cur->next;
		if(count % 10 == 0) printf("\n"); // break up lines for clarity
	}
	printf("\n");

	return;
}


// insert nodes at the tail of the list
node *insertTail(node *head, int a) {

        node *cur,*new;

	new = (node *)malloc(sizeof(node));
	new->num  = a;
	new->next = NULL;

        if (head == NULL) {
                // nothing in list, so create list
                return new;
        }

        cur = head;
        while (cur->next != NULL)
                cur = cur->next;

        cur->next = new;

	return head;
}

// Delete all numbers that are a multiple of a (excluding a itself)
node *deleteMultiples(node *head, int a) {
	int i;
	node *cur;
	node *prev;
	node *temp;

	cur = head;
	while(cur != NULL) {
		if ((cur->num != a)&&((cur->num %a) == 0)) {
			// divisible by a? delete node!
			prev->next = cur->next;
			temp = cur;
			cur = prev;
			free(temp);
			
		}
		prev = cur;
		cur  = cur->next;
	}

	return head;

}

int main(void) {
	int i;
	node *head,*cur;

	head = NULL;

	// populate the list
	for (i = 1; i <= SIZE; i++) {
		head = insertTail(head,i);
	}

	// delete non-primes, one multiple at a time
	for (i = 2; i < MSTOP; i++) {
		head = deleteMultiples(head,i);
	}

 	// display the results	
	printList(head);

	cur = head;
	// clean our mess
	while(head->next != NULL) {
		cur = head->next;
		free(head);
		head = cur;	 
	}

	free(head);

        return 0;
}

