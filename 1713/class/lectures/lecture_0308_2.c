/*---------------------------------------------------*/
/*  Name: Your Name Here                             */
/*                                                   */
/*  Brief description of program here                */
/*                                                   */
/*****************************************************/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  char data[20]="This is a string";
  char *str = "This is another string";
  char *str2;
  char *str3;
  
  data[0]='t';
  printf("%s\n",data);
  
  //*str = 't';
  //printf("%s\n",str);

  str2 = str;
  printf("%s\n",str2);
  
  str2 = (char *)malloc(40*sizeof(char));
  
  printf("Length of data = %d\n",strlen(data));
  printf("Length of str = %d\n",strlen(str));
  printf("Length of str2 = %d\n",strlen(str2));
  
  strcpy(str2,str);
  printf("%s\n",str2);
  
  str3=str2;
  if (strcmp(str3,str2)==0)
     printf("str3 equals str2\n");
  strcpy(str3,data);
  printf("%s\n",str3);
  
  strcat(str3,data);
  printf("%s\n",str3);
  
  data[strlen(data)]='a';
  printf("%s\n",data);
  
  printf("%d\n",strcmp(data,str));

  free(str2);
  return 0;
}

