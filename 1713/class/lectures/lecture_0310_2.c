/*---------------------------------------------------*/
/*  Name: Your Name Here                             */
/*                                                   */
/*  Brief description of program here                */
/*                                                   */
/*****************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

// count the number of words in a string
int countwords(char *s)
{
	
}

// convert all characters to uppercase
void uppercase(char *s)
{
  int i;
  for (i=0;i<strlen(s);i++)
	s[i]=toupper(s[i]);
}

void uppercase2(char *s)
{
  int i;
  for (i=0;s[i]!='\0';i++)
	s[i]=toupper(s[i]);
}

void uppercase3(char *s)
{
  int i;
  for (i=0;s[i]!='\0';i++)
	if (s[i]>='a' && s[i]<='z')
	  s[i]=s[i]-32;
}

void uppercase4(char *s)
{
  for (;*s!='\0';s++)
	if (*s>='a' && *s<='z')
	  *s=*s-32;
}

// count the number of digits in the string
int countdigits(char *s)
{
  int count = 0;
  while (*s != '\0')
  {
  	if (*s >= '0' && *s<='9')
  	   count++;
  	s++;
  }	
  return(count);	
}

// reverse the string and return it
char *reverse(char *s)
{
	
}

// count how many times c appears in s
int countletters(char *s, char c)
{
  int count = 0;
  while (*s != '\0')
  {
  	if (*s == c)
  	 count++;
  	s++;
  }
  return(count);	
}

// find and return the most frequent character in s
char mostfrequentcharacter(char *s)
{
	char ch;
	int count = 0;
	char *str=s;
	
	while (*s != '\0')
     {
	    if (*s != ' ' && countletters(str,*s)>count)
	    {
	        count = countletters(str,*s);
	        ch = *s;
	    }
	    s++;
     }
	return(ch);
}

// find the first nonrepeating character in s
char firstnonrepeatingcharacter(char *s)
{
	
}

int main()
{
  char data[25]="This is a string";
  char *str = "This is 12 also a string";
  char *str2;
  char *str3;
  
  str2 = (char*)malloc(40*sizeof(char));
  strcpy(str2,str);
  
  printf("Before: %s\n",str2);
  uppercase4(str2);
  printf("After: %s\n",str2);
  
  printf("Number of digits = %d\n",countdigits(str2));
  printf("Number of A's in str2 = %d\n",countletters(str2,'A'));
  printf("Most frequent character : %c",mostfrequentcharacter(str2));
  
  free(str2);
  return 0;
}

