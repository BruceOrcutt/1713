#include <stdio.h>
#include <string.h>

#define SIZE 50

char *reverse(char *str) {
	char temp[SIZE];
	int  i,j = 0;

	for (i = strlen(str)-1,j = 0 ; i >= 0;j++, i--) 
		temp[j] = str[i];
		
	
	return(temp);
}

int palindrome(char *str) {
	char *temp;
	int  i = 0;

	temp = reverse(str);
	for(i = 0; i < strlen(str)-1; i++) 
		if (str[i] != temp[i]) return 0;
	
	return (1);
}



int main(void) {
	char string[SIZE];

	
	printf("please give a string:\n");
	scanf("%s",string);
	//gets(&string,SIZE);
//	getline(string);
	printf("1. reversing %s: %s\n",string,reverse(string));
	printf("2. is it a palindrome? %d\n", palindrome(string));
}
