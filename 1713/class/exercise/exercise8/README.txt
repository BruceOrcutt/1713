Implement the following string functions
char *reverse(char *str)
int palindrome(char *str)
• The function reverse reverses the string that is given as parameter and returns
the string. For example for ”apple banana” it returns ”ananab elppa”.
• The function palindrome returns 1 if the parameter is a palindrome and 0 otherwise.
A string is a palindrome if it reads the same backwards and forwards. For
example ”racecar racecar” and ”abcd efe dcba” are palindromes.
