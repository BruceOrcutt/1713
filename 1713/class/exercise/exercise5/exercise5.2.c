#include <stdio.h>
#include <stdlib.h>

int issorted(int data[], int n) {
  int prev  = 0;
  int index = 0;

  for (index = 0; index < n; index++) {
    // no prev for first number checked
    if (index != 0) {
      if (prev > data[index]) return(0);  // EXIT POINT not sorted
    }
   prev = data[index];
  } // end for
  
  return(1);
  
} // issorted

int main(void) {
  int a[] = {1,2,3,4};
  int b[] = {4,3,2,1};
  int c[] = {1,1,2,3};
  int d[] = {-1,0,1};

  printf("a\tb\tc\td\n");
  printf("-------------------\n");
  printf("%d\t%d\t%d\t%d\n",issorted(a,4),issorted(b,4),issorted(c,4),issorted(d,3));
  
}
