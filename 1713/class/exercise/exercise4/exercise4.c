#include <stdio.h>
#include <math.h>

int figureIt(int a, int b) {
  
  if (b == 0) return 1;
  else {
    return(figureIt(a,b - 1) * a);
  }
} 

int main(void) {
  int a = 0;
  int b = 0;

  printf("Enter base and power: ");
  scanf("%d %d",&a, &b);

  printf("Result is : %d\n",figureIt(a,b));
}
