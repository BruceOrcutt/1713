#include <stdio.h>
#include <math.h>

#define SIZE 50

void precomputesummation(int data[], int n) {
  int index = 0;

  for(index = 0; index < n; index++) {
    if(index == 0) data[index] = 0;
    else data[index] = data[index-1] + index;
  } // end for
} // end precompute

void findsummation(int data[], int n, int a, int b) {
  if ((a > b)||(b > n)||(a < -1)) printf("ERROR: b outside array");  // ERROR bigger than array
  else if (a == -1) printf("Exit signaled\n");
  else {
    printf("SUM from %d to %d is: %d\n",a,b,data[b] - data[a-1]); 
  }
  
} // end findsum

int main(void) {
  int data[SIZE];
  int a = 0;
  int b = 0;

  precomputesummation(data, SIZE);
  

  while (a > -1) {
    printf("Provide the sum beginning and endpoint. -1 -1 to exit\n");
    scanf("%d %d",&a,&b);
    findsummation(data,SIZE,a,b);
  }


  return 0; // EXIT POINT
} // end main
