#include <stdio.h>
#include <math.h>

int main(void) {
  // 1713 Sec 2
  // Exercize 1.  Part 1
  // By: Bruce A Orcutt
  // 1/18/2017
  // Description: asks for a 3 digit number. if not 3, ask for a new number
  // if given 3 digits, provide the reverse order of the digits

  // initialization
  int index	= 0; // index for for loop
  int result	= 0; // for accumilating results 
  int value	= 0; // value user provides
  int temp        = 0;
  int original    = 0;
  
  // get the value
  while((value >= 1000)||(value<100)) {
    printf("What is the number to reverse?\n");
    scanf("%d",&value);
    if ((value >= 1000)||(value<100)) {
      printf("Number must be exactly 3 digits\n");			
    }
  }
  
  original = value;
  
  // loop
  while(value > 0) {
    temp    = value % 10;  // get the rightmost digit
    value  /= 10;          // move over a digit
    result *= 10;          // make room for the new digit
    result += temp;        // add the digit into the newly created space
    
  }
  
  // output
  printf("%d reversed is %d\n",original,result);

}
