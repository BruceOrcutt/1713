#include <stdio.h>
#include <math.h>

int main(void) {
  // initialize
  int max  = 0;
  int a    = 0;
  int b    = 0;
  int c    = 0;
  int read = 0;

  // get the values
  while (read < 3) {
    printf("Please provide 3 numbers:\n");
    read = scanf("%d %d %d",&a,&b,&c);
    if (read < 3) {
      printf("Less than 3 numbers provided. Please provide exactly 3 numbers\n");
    }
  }
  
  // compare the values
  if (a < b) {
    max = b;
  }
  else {
    max = a;
  }
  
  if (!(c < max)) {
       max = c;
  }
  
  // print the result
  printf("\nThe Maximum of (%d) (%d) (%d) is: %d\n",a,b,c,max);

}
