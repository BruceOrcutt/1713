#include <stdio.h>
#include <stdlib.h>

int  main(void){

  FILE *fp; 
  int  index = 0;
  int  max = 0;
  int max2 = 0;
  int temp = 0;
  
  fp = fopen("testdata.txt","r");

  if (!fp) {
    printf("Could not open file!\n\n");
    exit(-1);
  }

  do {
    if(fscanf(fp,"%d",&temp)) index++;
    if(temp > max) {
      max2 = max;
      max  = temp;
    }
    else if (temp > max2) {
      max2 = temp;
    }
    printf ("index %d number %d max %d max2 %d\n\n",index,temp, max,max2);
  } while (!feof(fp));
  
  if (index == 0) {
    printf("No integers found!\n\n");
    exit(-1);
  } else if (index == 2) {
    printf("Only 1 integer found\n\n");
    exit(-1);
  }

  printf("2nd maximum is %d\n",max2);
  
}
