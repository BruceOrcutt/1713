CS 1713
Introduction to Computer Programming II
Exercise 7
1. Write a program that finds the majority element in an array if it exists. Majority
element is the element which appears in more than half of the array locations. Declare
an array of size 7 and read the array elements from the user. Then, compute and
print the majority element if it exists. In an array of size 7, an element has to appear
in at least 4 array locations in order to be majority element. Consider the following
example.
0 1 2 3 4 5 6
3 3 2 1 3 2 3
Majority element in this array is 3 since 3 appears 4 times and array size is 7 (0-6).
Sample execution of the program for this array is given below
Enter 7 numbers
3 2 3 3 2 3 1
Majority = 3
In some cases majority element does not exist. Following example shows this.
0 1 2 3 4 5 6
1 2 4 3 1 2 3
There is no majority element in this array. Maximum count in the array is 2. However,
4 is needed for majority. Sample execution of the program is given below
Enter 7 numbers
1 2 2 3 4 3 1
No Majority
One approach to this problem is to count how many times each element appears in the
array. Find the maximum occurrence and compare it with 4. If maximum occurrence
in the array is ≥ 4 then corresponding element is majority. If maximum occurrence is
< 4 then there is no majority element.
