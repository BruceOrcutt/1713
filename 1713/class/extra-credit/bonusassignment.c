#include <stdio.h>
#include <math.h>
#define P printf
#define L " Loop\n  Loop Result = "
#define F "  Formula = "
#define K k=1;r=0;
int main(){int k,n,r;float t;P("Enter n: \n");scanf("%d",&n);K for(;k<=n;k++)r+=k*k*k;P("For%s%d\n%s%d\n",L,r,F,n*n*(n+1)*(n+1)/4);t=0.0;K while(k<=n)t+=1.0/(pow(2.0,k++));P("While%s%f\n%s%f\n",L,t,F,1-1.0/pow(2,n));K do r+=k*(k++); while (k<=n);P("Do while%s%d\n%s%d\n",L,r,F,n*(n+1)*(n+2)/3);return 0;}
