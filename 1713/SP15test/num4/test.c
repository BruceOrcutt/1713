#include <stdio.h>

int main()
{
  int n,i;
  float sum = 0;

  printf("Enter n\n");
  scanf("%d",&n);

  for (i=1; i<=n; i=i+1)
    sum = sum + 1.0/i + 1.0/(2*i);

  printf("Sum = %f\n",sum);

  //system("pause");
  return(0);
}
