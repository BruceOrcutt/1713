#include <stdio.h>
#include <stdlib.h>

#define SIZE 100



int main(void) {
	int *info[100];
	int i,j;
	int interval, max, count, choice;

	for (i = 0; i < SIZE; i++) {
		 info[i] = (int *)malloc(sizeof(int));
		*info[i] = rand()%1000;
	}

	printf("What interval do you wish to test?\n");
	scanf("%d",&interval);

	max = 0;count = 0;
	for (i = 0; i < SIZE; i++) {
		for (j = 0; j < SIZE; j++)
			// each one needs to be checked each time, so start at 0
			if ((*info[j] >= *info[i])&&(*info[j]<=*info[i]+interval))
				count++;

		if (count > max) {
			choice = i;
			max = count;
		}
		count = 0;
	}

	printf("Best placement of interval %d is at number %d  with %d elements\n",interval,choice+1,max);
	printf("Given array below:\n");

	for (i = 0; i < SIZE; i++) {
		printf("%d\t",*info[i]);
		free(info[i]);
	}
	printf("\n");

}

