#include <stdio.h>
#include <stdlib.h>
#include <math.h>
     ////////////////////////////
    // Program: Recitation 3
   //  By: Bruce Orcutt
  //   Function: given a number provided on the command line, reverse the order
 // of the digits, and print the number of the digits
///////////////////////////////

int numDigits(int x) {
  // counts the number of digits in an int
  int value = x;
  int index = 1;

  for(index = 1; value > 0; index++) {
    value /= 10;
  }
  return(index-1);

}


void reverseDigits(int x) {
  // reverses the digits of an int
  int value = x;
  int temp  = 0;
  

  while(value > 0) {
    temp  = temp*10 + value%10; // move number over 1 place, add the new digit
    value /= 10; // remove the digit just processed
  }
  
  printf("%d\n",temp);

}

int main(int argc, char *argv[]) {

  int value = 0;

  // verify user entered exactly one input
  if(argc != 2) {
    printf("Usage: %s integer\n",argv[0]);
    exit(-1);  // EXIT POINT
  }

  reverseDigits(atoi(argv[1]));
  printf("%d\n",numDigits(atoi(argv[1])));
  
}
