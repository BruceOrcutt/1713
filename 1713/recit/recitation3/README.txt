CS 1713
Introduction to Computer Programming II
Recitation 3
1. (100 pts) Write a program to implement the following functions recursively.
void reverseDigits(int number)
int numDigits(int number)
• The function reverseDigits prints the digits of a number in reverse order. For
example, for 12345 it prints 54321 and for 287 it prints 782.
• The function numDigits finds the number of digits in a number and returns it.
For example, for 12345 it returns 5 and for 287 it returns 3.
• Use argc and argv in your program. You need to use atoi to convert the string
parameter to an integer and include < stdlib.h > to use atoi.
Sample output for this recitation is as follows
fox01> recitation3 34567
76543
5
Output consists of two numbers written on separate lines. First output is for reverseDigits
and second output is for numDigits. Name your program recitation3.c
Submit your program electronically using the blackboard system
