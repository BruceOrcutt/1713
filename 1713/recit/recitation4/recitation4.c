#include <stdio.h>

#define ARRAY_SIZE 50  // how many of the series are we calculating and storing?

     ///////////////////////////
    // Program: recitation4
   //  By:      Bruce Orcutt (csz860)
  //   Purpose: First calculates the Fibonacci sequence, up to 50, and stores the values in an array
 //    it then takes ints, via STDIN, and looks up the appropriate value from the array to STDOUT
//////////////////////////////

void storeFib(double fibArray[],int size) {
  // this function calculates the Fibonacci sequence, up to size, storing the values in an array of doubles
  int index = 0;

  // check to make sure we are calculating at least one value to store
  if (size < 1) return;  // EXIT POINT

  
  for(index = 0; index <= size; index++) {
    // first the two special cases
    if(index == 0) fibArray[index] = 1;
    else if(index == 1) fibArray[index] = 1;
    
    // otherwise follow the regular pattern
    else fibArray[index] = fibArray[index-1] + fibArray[index-2];
  
  } // end for
  
  
} // end storeFib

int main(void) {
  double fibArray[ARRAY_SIZE];
  int    input = 0;

  // load up the values
  storeFib(fibArray,ARRAY_SIZE);

  // get input until signaled via -1 to stop
  while (input != -1) {
    scanf("%d",&input);
    if (input > 0) printf("%d\t%.0lf\n",input,fibArray[input]);
  } // end while
  
  return 0;
}
