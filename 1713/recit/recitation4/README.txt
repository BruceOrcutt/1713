CS 1713
Introduction to Computer Programming II
Recitation 4
1. (100 pts) Write a program to precompute the Fibonacci numbers and store them in
an array. Fibonacci numbers are defined as follows F ib(0) = 1, F ib(1) = 1 and
F ib(i) = F ib(i − 1) + F ib(i − 2). Recursive implementation of Fibonacci is very slow
and precomputing them and storing them in an array makes it easier to answer queries.
Allocate an array of size 50 to store Fibonacci numbers and store the i
th Fibonacci
number at index i. Since Fibonacci numbers increase rapidly use double as element
type for array. Have a loop in your program to read i and print i and i
th Fibonacci
number. Use −1 to quit the loop. Use input redirection to test your program. Note
that your program reads from the user using scanf and input redirection feeds the file
contents to your program. You don’t have to use any file operations in your program.
Consider the following file a.txt
4
10
20
15
5
-1
Sample output for this recitation using input redirection is as follows
fox01> recitation4 < a.txt
4 5.000000
10 89.000000
20 10946.000000
15 987.000000
5 8.000000
Name your program recitation4.c
Submit your program electronically using the blackboard system
