#include <stdio.h>
#define debug 0

// Program:   Recitation1
// For:       CS1713 Recitation Section 4
// By:        Bruce A. Orcutt
// abc123:    csz860
// Explanation: a program to find the largest digit in a four digit number [1000..9999. Reads the number from the user, finds the largest digit and prints it. Assumes that the user enters a 4 digit number. 


int main(void) {
    //////////////////////////
   // variable dictionary  //
  //////////////////////////
  int value = 0;  // The four digit number from the user
  int temp  = 0;  // for destroying the number to get the digits
  int digit = 0;  // current digit to evaluate
  int max   = 0;  // maximum value
   
    //////////
   // code //
  //////////

  // get the number
  printf("Please provide the 4 digit number: ");
  scanf("%d",&value);
  temp = value;  // prepare for the loop
  
  // process the digits
  while (temp > 0) {
    digit  = temp % 10;  // get the next digit
    temp  /= 10;       // go to the next digit for next round
    
    if (debug) printf("%d is digit \n %d is max\n",digit, max);
    
    if (digit > max) {     
      max = digit;  // see if we have a new max
    } // end if
  } // end while

  // output
  printf("\nThe largest digit of %d is %d\n",value,max);


} // end of Main
