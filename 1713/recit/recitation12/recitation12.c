#include <stdio.h>
#include <stdlib.h>

#define SIZE 100

struct Node {
	int a;
	struct Node *next;
};
typedef struct Node node;

// insert nodes at the tail of the list
node *insertTail(node *head, int a) {

        node *cur,*new;

        new = (node *)malloc(sizeof(node));
        new->a  = a;
        new->next = NULL;

        if (head == NULL) {
                // nothing in list, so create list
                return new;
        }

        cur = head;
        while (cur->next != NULL)
                cur = cur->next;

        cur->next = new;

        return head;
}

node *delete(node *head, int k) {
	int i;
	node *prev,*cur;

	cur = head;
	
	if (head->a == k) return head->next;
	
	
	while(cur!=NULL) {
		if(cur->a == k) {
			prev->next = cur->next;
			free(cur);
			return head;
		}
		else {
			prev = cur;
			cur = cur->next;
		}
	}
	
	return head;
}

node *recursivedelete(node *head, int k) {
}


void printList(node *head) {
	node *cur;
	
	cur = head;
	while(cur!=NULL) {
		printf("%d \t",cur->a); 
		cur = cur->next;
	}
	return;	
}

int main(void) {
	int i;
	node *head,*cur;

	head = NULL;

	for (i=0; i<SIZE;i++) head = insertTail(head,i); 

	printf("enter a value to delete\n");
	scanf("%d",&i);

	head = delete(head,i);
	
	printList(head);

        // clean our mess
        while(head->next != NULL) {
                cur = head->next;
                free(head);
                head = cur;
        }

        free(head);


	
}








