#include <stdio.h>
#include <stdlib.h>

#define PI 3.14
#define SIZE 50

     //////////////////////////////
    // Program: recitation10
   // By: Bruce A. Orcutt
  // Date: 4/5/2017
 // Purpose:  Allocates 50 random circles, then returns the area of the largest
////////////////////////////////
struct circle {
	double x,y,r;
};	

double rand_float(double a,double b) { 
	return ((double)rand()/RAND_MAX)*(b-a)+a;
}

double area(double r) {
	return PI*r*r;
}

int main(void) {
	struct circle *a[SIZE];
	int i,j;
	double max;

	max = 0.0;
	j = -1;

	for(i=0;i<SIZE;i++) {
		a[i] = (struct circle *) malloc(sizeof(struct circle));
		a[i]->x = rand_float(100,900);
		a[i]->y = rand_float(100,900);
		a[i]->r = rand_float(0,100);
		
		if (area(a[i]->r)>max) {
			max = area(a[i]->r);
			j = i;
		}	
	}
	
	printf("Circle with largest area (%lf) has \n",max);
	printf("center (%lf,%lf) and radius %lf\n",a[j]->x,a[j]->y,a[j]->r);

	for(i=0;i<SIZE;i++) free(a[i]);
	
	return 0;

}
