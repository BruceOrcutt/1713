#include <stdio.h>

       ////////////////////////////////////////
      // Recitation 6
     //  By: Bruce Orcutt (csz860)
    //   Date: 3/1/2017
   //    Purpose: Read an array of size 7 from the user, compute the smallest
  //     positive integer that does not appear in the array or can not be 
 //      formed by the sum of two numbers in the array.
///////////////////////////////////////////////     

int issumof2(int data[], int size, int number) {
// returns 1 if the number is the sum of two other array elements, 0 otherwise
    int i = 0;
    int j = 0;

    for(i = 0; i < size; i++) 
	for(j = 0; j < size; j++)
	   if ((data[i] + data[j]) == number) return 1; 
        
    return 0;    
}

int inarray(int data[],  int size, int number) {
// returns 1 if the number is in the array, 0 oherwise
   int i = 0;

   for(i = 0; i < size; i++) 
	if (data[i] == number) return 1;
   
   return 0;
}

int main(void) {
    int data[7] = {0,0,0,0,0,0,0};
    int smallest = 0;
    int i,count = 0;

    printf("Please provide 7 integers\n");
    for(i = 0; i < 6; i++) {
	scanf("%d",&data[i]);
    }
   
   for(count = 1; ; count++) {
	if (inarray(data,7,count)) continue;  	
        if (issumof2(data,7,count)) continue;	
        break; // end once we have a number that passes the tests above
   } 

   printf("Smallest positive Integer = %d\n",count);
	
}
