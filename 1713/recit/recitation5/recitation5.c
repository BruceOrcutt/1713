#include <stdio.h>
#include <math.h>

#define SIZE 50

      //////////////////////////////////
     // Name: recitation5
    //  By: Bruce A. Orcutt (csz860)
   //   Date: 2/22/2017
  //    Purpose: Given a string of ints, terminated by -1, compute the minimum difference 
 // between members
//////////////////////////////
int main(void) {
  int items[SIZE];    // items read
  int count   = 0;    // count of items user supplied
  int diff    = 0;    // curent smallest difference
  int val1    = 0;    // one number of chosen pair
  int val2    = 0;    // second number of chosen pair
  int i,j     = 0;    // index for loop
  int item    = 0;    // current item read

  // collect the items, ending when -1 detected
  for(i = 0; item != -1; i++) {
    scanf("%d",&item);
    if (item != -1) items[i] = item;
  }

  // 2 because we want 0 based, and i increments also one beyond last item
  count = i-2;

  // check we have at least 2 items to calculate a difference between!
  if (count < 1) {
    printf("Error, must have a minimum of two numbers!\n");
    return -1;
  }

  // set initial values
  diff = abs(items[0] - items[1]);
  val1 = items[0];
  val2 = items[1];

  // loop over all numbers, comparing between
  for(i = 0;i <= count; i++)
    for(j = i+1;j <= count; j++) 
      if (abs(items[i] - items[j]) < diff) {
	// if we have a new minimum, store the vales
	diff = abs(items[i] - items[j]);
	val1 = items[i];
	val2 = items[j];
      } // end if
    
  // output to user
  printf("Closest pair: %d and %d, Difference: %d\n",val1,val2,diff);


} // end main
