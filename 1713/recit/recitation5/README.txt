CS 1713
Introduction to Computer Programming II
Recitation 5
1. (100 pts) Write a program to find the closest pair of numbers entered by the user.
Closest pair is the pair of numbers whose difference is minimum among all the pairs.
Consider the following set as an example {8, 2, 17, 4, 25, 32, 20}. Closest pair in this
set is 2 and 4 with difference of 2. All the other pairs have differences > 2. If you
add 24 to this set to get {8, 2, 17, 4, 25, 32, 20, 24}, then closest pair is 24 and 25 with
difference of 1. All the other pairs have differences > 1. If there is more than 1 pair
with same difference, return the first one. Have a loop in your program to read the
numbers and use −1 to quit the loop. Use an array of size 50 to insert the numbers.
Use input redirection to test your program. Note that your program reads from the
user using scanf and input redirection feeds the file contents to your program. You
don’t have to use any file operations in your program. Consider the following file a.txt
8 2 17 4 25 32 20 -1
Sample output for this recitation using input redirection is as follows
fox01> recitation5 < a.txt
Closest pair: 2 and 4, Difference: 2
Name your program recitation5.c
Submit your program electronically using the blackboard system
