#include <stdio.h>
#include <string.h>

// arbitrarily large size for array/buffer. 
#define SIZE 30000

     /////////////////////////////////
    //  Program: recitaiton7
   //   By: Bruce Orcutt (csz860)
  //    Date: 3/13/2017
 //     Purpose: given a line of character data, reverse each space seperated string found 
/////////////////////////////////////

void reverse(char *str, int i, int j) {
// Given a string, a starting and ending index, reverse the string at the provided boundaries 
	int  k,m = 0;    // for loop index
	char t   = '\0'; // temp for swapping letters
	
		
	// looping from both sides, reverse te array 
	for (k = j,m = i;k > m;k--,m++) {
		t      = str[k];
		str[k] = str[m];
		str[m] = t;	
	}  	
	
}

int  main(void) {
	int  q,r = 0;   // indexes to discover spaces
	int  l   = 0;   // length of entire string given
	char s[SIZE];   // original string 

	// get line
	printf("Please provide a string:\n");
	
	// get the line and it's size, using s's size as a buffer size
	fgets(s,sizeof(s),stdin); 	
	l = strlen(s);
	
	// lop off newline by replacing with NULL to terminate string
	if (s[l-1] == '\n') s[l-1] = '\0';
	
	l = strlen(s); // recalculate length, as we may have shortened the array

 	// find the spaces, invoke reverse on each token found
	while(q<=l) {
		if ((s[q] == ' ')||(s[q] == '\0')) {
			reverse(s,r,q-1); //reverse from previous end
			r = q+1; // keep track of end of this token
			q++;
		} else q++;
		
	} 
			
	
//	reverse(s,0,l-1);
	printf("Reversed String:\n%s\n",s);


}

