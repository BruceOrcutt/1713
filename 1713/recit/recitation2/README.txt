CS 1713
Introduction to Computer Programming II
Recitation 2
1. (100 pts) Write a program to find all the triangles with integer side lengths and a user
specified perimeter. Perimeter is the sum of all the sides of the triangle. Integers a,
b and c form a triangle if the sum of the lengths of any two sides is greater than the
third side. Your program should find all the triples a, b and c where a + b + c is user
specified perimeter value. Print each triple only once in increasing order. For example,
3, 4 and 5 forms a triple with perimeter 12. Only print 3 4 5 instead of printing all
the permutations of the triple.
Sample output for this recitation is as follows
Enter perimeter
24
Triangles with perimeter 24
2 11 11
3 10 11
4 9 11
4 10 10
5 8 11
5 9 10
6 7 11
6 8 10
6 9 9
7 7 10
7 8 9
8 8 8
Test your program with different values for perimeter. Number of such triangles is know
as Alcuin’s sequence and list of values is available at http://oeis.org/A005044/list
You need to use nested loops for this problem. Name your program recitation2.c
Submit your program electronically using the blackboard system
:wq
