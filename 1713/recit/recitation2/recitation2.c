#include <stdio.h>
#include <math.h>

int main(void) {
  int perimeter = 0;
  int a = 0;
  int b = 0;
  int c = 0;

    /////////////////////////////////////
   // Program: recitation2.c
  //` By:  Bruce A. Orcutt (csz860)
 //   Purpose: Given the perimeter of a triangle, return all possible side lengths
////////////////////////////////////////

  printf("Enter perimeter ");
  scanf("%d",&perimeter);
  printf("Triangles with perimeter %d\n",perimeter);
  
  for (a = 1; a<perimeter/2 ; a++) {
    for (b = a; b<perimeter/2 ;b++) {
      for (c = b; c<perimeter/2; c++) {
	if (((a+b+c) == perimeter)) printf("%d %d %d\n",a,b,c);
      } // for c 
    } // for b
  } // for a


  return 0;
  
  
} // main
