#include <ctype.h>
#include <string.h>
#include <stdio.h>

#define SIZE 20000
     /////////////////////////////
    // Program: Recitation9
   // By: Bruce Orcutt
  // Date: 3/29/2017
 // Purpose: Given a sentance and a character, report all words containing the character
//////////////////////////////

int main(void) {
	char	c;	
	char    s[SIZE];
	int	i,j = 0;   //counters
	char    t[SIZE];   // tokens
	char    u[SIZE];  // upper case

	// ask the user for the input
	printf("Enter a sentance\n");
	fgets(s,SIZE,stdin);
	printf("Enter a character\n");
	scanf("%c",&c);

	printf("Words containing %c are:\n",c);

	// loop over entrire sentance
	for (i = 0; i < strlen(s)+1;i++) {
		t[j] = s[i];  // save the current character to the temp array
	
		// check for word boundry
		if ((t[j] == ' ')||(t[j] == '\0')) {
			t[j] = '\0'; // terminate string
			u[j] = '\0';

			// use toupper to ignore case
			if (strchr(u,toupper(c))) 
				printf("%s\n",t);
		 	j = 0;  // word boundry hit so reset counter for t,u	 		
		} else {
			// otherwise still building current word.  
			// store next character in upper case string			
			u[j] = toupper(t[j]);
			j++;
		}
	}	
					

	
}
