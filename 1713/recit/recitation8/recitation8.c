#include <stdio.h>
#include <stdlib.h>

#define SIZE 10 
       ///////////////////////
      // Program: recitation8
     // By:      Bruce Orcutt
    // Date:    3/24/17
   // Purpose: Given an array of integers given at the command line
  // determine the average, then tell the user how many numbers are above
 // the average
///////////////////////////

double average(int *ptr, int size) {
// Given array of integers, determine and return the average
	double 	avg	= 0.0;
	int 	i	= 0;

	for(i = 0; i <= size; i++) 
		avg += (double)ptr[i];
	avg /= size;

	return avg;			
}

int aboveaverage(int *ptr, int size, double average) {
// determine how many numbers in an int array are above average of all numbers
	int i     = 0;
	int count = 0;

	for(i = 0; i <= size; i++) 
		if (ptr[i]>average) count++;
	return count;
}

int main (int argc, char *argv[]) {
	FILE   *F;		// the file of integers
	int     done  = 0;	// loop terminator
	int     count = 0;	// count the number of values
	int     mem   = 0;	// keep track of amount of elements allocated
	int	tInt  = 0; 
	int    *array;		// array of integers	
	int    *temp;		// temporary for using realloc in case of NULL
	double  avg   = 0.0;	// average of all values

        // ensure proper number of arguments
        if(argc != 2) {
                printf("ERROR: must have one arguments\n");
                printf("Usage: %s filename \n",argv[0]);
                return -1;  // EXIT POINT
        }

        // open the file specified by the user
        if (!(F = fopen(argv[1],"r"))) {
                printf("ERROR: unable to open file %s\n",argv[1]);
                return -3;  // EXIT POINT
        }
	
	printf("Allocated %d integers\n",SIZE);
	if(!(array = (int *)calloc(SIZE,sizeof(int)))) printf("ERROR allocating memory!\n");
	
	mem   = SIZE;
	
	while(!done) {
		fscanf(F,"%d",&tInt);
                // check if we've reached the end of the file   
                if (feof(F)) {
                        done = 1;
                }

               // do we need more space? Do we have a value? 
		if ((!done) && (count>mem-1)) {
                        temp =  (int *) realloc(array,mem*2*sizeof(int));
			
			// make sure we got what we requested!
                        if   (!temp) printf("Error allocating\n");
                        else array = temp;

                        mem *= 2;
                        printf("Reallocated to %d integers\n",mem);
                }
		if (!done) {
			array[count] = tInt;
			count++;
		}
	} 	

	// get the average and 	
	avg = average(array,count);
	printf("%d elements are above average of  %lf\n",aboveaverage(array,count,avg),avg);

	// free the allocated memory
	free((void *)array);
	printf("Dynamic array freed\n");	
	return 0;
}
